%vert Generates reference function for DIH version of SVM.

% number of phases, must be divisible by six.
n_phases = 4*6;
% maximal duration of PWM duty cycle.
m_pwm = 1.0;

f = round(n_phases / 6);
ta = []; tb = [];
for k = 0:(f-1)
	alpha = pi/3 * k/f;
	ta(k+1) =  sin(pi/3 - alpha);
	tb(k+1) =  sin(alpha);
end

ref = m_pwm*horzcat( (ta+tb)/2, (ta-tb)/2, -(ta+tb)/2, -(ta+tb)/2, (tb-ta)/2, (ta+tb)/2 );

str = cstrcat(' { ', sprintf('%.5fr, ', ref), ' };');
	
n_phases
str
