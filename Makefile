include Makefile.common
LDFLAGS=$(COMMONFLAGS) -fno-exceptions -ffunction-sections -fdata-sections -L$(LIBDIR) -nostartfiles -Wl,--gc-sections,-Tlinker.ld

#LDLIBS+=-lm
LDLIBS+=-lstm32

#STARTUP=startup.c

TARGET=main

all: $(TARGET)
	$(CC) -o $(PROGRAM).elf $(LDFLAGS) \
		-Wl,--whole-archive \
			src/$(TARGET).a \
		-Wl,--no-whole-archive \
			$(LDLIBS)
	$(OBJCOPY) -O ihex    $(PROGRAM).elf $(PROGRAM).hex
	$(OBJCOPY) -O binary $(PROGRAM).elf $(PROGRAM).bin

.PHONY: libs src clean
libs:
	$(MAKE) -C libs $@

main: libs
	$(MAKE) -C src $@ 

test_leds: libs
	$(MAKE) -C src $@ 

test_motor: libs
	$(MAKE) -C src $@ 

test_rtc: libs
	$(MAKE) -C src $@ 

test_ekf: libs
	$(MAKE) -C src $@ 

test_motfb: libs
	$(MAKE) -C src $@ 

test_disp: libs
	$(MAKE) -C src $@ 

test_fixed: libs
	$(MAKE) -C src $@ 

cleanall: clean
	$(MAKE) -C libs $@

clean:
	$(MAKE) -C src $@
	rm -f $(PROGRAM).{elf,hex,bin}

program:
	stm32flash -w $(PROGRAM).bin -v -g 0x0 /dev/ttyUSB0
