figure(1)
plot(t, x(:,2), '-;vel;', T/NpS, omega, '-o;vel_{est};', T/NpS, omega_s, '-x;vel_s;', t, x(:,3), '-;accel;', T/NpS, theta, '-o;accel_{est};', T/NpS, theta_s, '-x;accel_s;')

figure(2)
err = x(T(2:end), 2) - omega(2:end);
nerr = sqrt(p11(2:end));
rerr = (x(T(2:end), 2) - omega(2:end)) ./ omega(2:end);

k = (2:length(T))';
plot(k, err, ';e;', k, nerr, ';sigma_e;')

figure(3)
k = (1:length(T))';
plot(k, nu./omega, ';e_{rel};', k, nu_s./omega, ';e_{rel s};')
