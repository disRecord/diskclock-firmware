#ifndef  LED_RGB_H
#define  LED_RGB_H

#include "disk_display_config.h"
#include "disk_display.h"

void led_rgb_init(void);

void led_rgb_deinit(void);

void led_rgb_set_pwm(diskd_pixel_color_t color);

void led_rgb_set_sector(uint8_t sector);

void led_rgb_set_sectors( uint16_t enabled_sectors );

#endif  /*LED_RGB_H*/
