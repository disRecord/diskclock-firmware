#ifndef  AUDIO_H
#define  AUDIO_H

#include "bool.h"

#define AUDIO_SAMPLE_RATE 8000

#define AUDIO_APB2Periph	(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO)
#define AUDIO_APB1Periph	(RCC_APB1Periph_TIM6|RCC_APB1Periph_DAC)

#define AUDIO_TIMER			TIM6

#define AUDIO_DAC_PIN		GPIO_Pin_5	
#define AUDIO_DAC_PORT		GPIOA

#define AUDIO_DAC_CHANNEL	DAC_Channel_2
#define AUDIO_DAC_TRIGGER	DAC_Trigger_T6_TRGO


void audio_init(void);

void audio_set_noise(BOOL state);

void audio_deinit(void);


#endif  /*AUDIO_H*/
