#include <ctype.h>

#include "font.h"

//CLOCK FONT
//TODO Rewrite without GCC extension.

static const uint16_t font_symbol_0[] = {
	0b0000000000,
	0b0011111100,
	0b0100000010,
	0b0100000010,
	0b0011111100,
};
static const uint16_t font_symbol_1[] = {
	0b0000000000,
	0b0000000000,
	0b0000001000,
	0b0000000100,
	0b0111111110,
};
static const uint16_t font_symbol_2[] = {
	0b0000000000,
	0b0110000100,
	0b0101000010,
	0b0100100010,
	0b0100011100,
};
static const uint16_t font_symbol_3[] = {
	0b0000000000,
	0b0100010010,
	0b0100010010,
	0b0100010010,
	0b0011101100,
};
static const uint16_t font_symbol_4[] = {
	0b0000000000,
	0b0000110000,
	0b0000101100,
	0b0000100010,
	0b0111111110,
};
static const uint16_t font_symbol_5[] = {
	0b0000000000,
	0b0100011110,
	0b0100010010,
	0b0100010010,
	0b0011110010,
};
static const uint16_t font_symbol_6[] = {
	0b0000000000,
	0b0011111100,
	0b0100010010,
	0b0100010010,
	0b0011100010,
};
static const uint16_t font_symbol_7[] = {
	0b0000000000,
	0b0111000010,
	0b0000100010,
	0b0000010010,
	0b0000001110,
};
static const uint16_t font_symbol_8[] = {
	0b0000000000,
	0b0011101100,
	0b0100010010,
	0b0100010010,
	0b0011101100,
};
static const uint16_t font_symbol_9[] = {
	0b0000000000,
	0b0100011100,
	0b0100100010,
	0b0100100010,
	0b0011111100,
};
static const uint16_t font_symbol_colon[] = {
	0b0000000000,
	0b0000000000,
	0b0011001100,
	0b0000000000,
	0b0000000000,
};
static const uint16_t font_symbol_space[] = {
	0b0000000000,
	0b0000000000,
	0b0000000000,
	0b0000000000,
	0b0000000000,
};

static const uint16_t * font_digit_table[] = {
	font_symbol_0,
	font_symbol_1,
	font_symbol_2,
	font_symbol_3,
	font_symbol_4,
	font_symbol_5,
	font_symbol_6,
	font_symbol_7,
	font_symbol_8,
	font_symbol_9,
};

font_symbol_t font_get_symbol(uint16_t c)
{
	if (isdigit(c)) {
		return font_digit_table[c - '0'];
	}
	switch (c) {
		case ':':
			return font_symbol_colon;
		default:
			return font_symbol_space;
	}
}

