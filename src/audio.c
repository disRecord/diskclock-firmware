#include "stm32f10x_conf.h"

#include "config.h"
#include "audio.h"


void audio_init(void)
{
	
	RCC_ClocksTypeDef clock_freq;
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_BaseInit;
	DAC_InitTypeDef DAC_InitStructure;

	RCC_APB1PeriphClockCmd( AUDIO_APB1Periph, ENABLE);
	RCC_APB2PeriphClockCmd( AUDIO_APB2Periph, ENABLE);

	// Init DAC trigger timer	
	RCC_GetClocksFreq(&clock_freq);
	TIM_TimeBaseStructInit(&TIM_BaseInit);
	TIM_BaseInit.TIM_Period = clock_freq.PCLK1_Frequency / AUDIO_SAMPLE_RATE - 1;
	TIM_BaseInit.TIM_Prescaler = 0;
	TIM_TimeBaseInit( AUDIO_TIMER, &TIM_BaseInit );
	// Set trigger mode
	TIM_SelectOutputTrigger( AUDIO_TIMER, TIM_TRGOSource_Update );

	// DAC pin configuration
	GPIO_StructInit( &GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = AUDIO_DAC_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init( AUDIO_DAC_PORT , &GPIO_InitStructure);
	
	// DAC initialization
	DAC_InitStructure.DAC_Trigger = AUDIO_DAC_TRIGGER;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_Noise;
	DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bits10_0;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
	DAC_Init(AUDIO_DAC_CHANNEL, &DAC_InitStructure);

	//DAC_SetChannel2Data(DAC_Align_8b_R, 128);
}


void audio_set_noise(BOOL state) 
{
	if (state) {
		TIM_Cmd(AUDIO_TIMER, ENABLE);
		DAC_Cmd(AUDIO_DAC_CHANNEL, ENABLE);
	}
	else {
		TIM_Cmd(AUDIO_TIMER, DISABLE);
		DAC_Cmd(AUDIO_DAC_CHANNEL, DISABLE);
	}
}

void audio_deinit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	DAC_DeInit();
	TIM_DeInit(AUDIO_TIMER);

	GPIO_StructInit( &GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = AUDIO_DAC_PIN;
	GPIO_Init( AUDIO_DAC_PORT , &GPIO_InitStructure);
}

