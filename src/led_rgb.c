#include "stm32f10x_conf.h"

#include "disk_display_config.h"
#include "led_rgb.h"

/**
 * @brief Init RGB pwm modulation.
 * Setup PWM modulation for RGB led. 
 **/
void led_rgb_init(void) 
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_BaseInit;
	TIM_OCInitTypeDef TIM_OCInit;

	RCC_APB1PeriphClockCmd( LED_APB1Periph, ENABLE);
	RCC_APB2PeriphClockCmd( LED_APB2Periph, ENABLE);

	// LED CHANNELS SETUP
	GPIO_InitStructure.GPIO_Pin = LED_CH1 | LED_CH2 | LED_CH3 | LED_CH4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_SetBits( LED_CH_PORT, LED_CH1 | LED_CH2 | LED_CH3 | LED_CH4 );
	GPIO_Init( LED_CH_PORT , &GPIO_InitStructure);

	// LED RGB SETUP
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

	// R pin
	GPIO_InitStructure.GPIO_Pin = LED_R;
	GPIO_ResetBits( LED_R_PORT, LED_R );
	GPIO_Init( LED_R_PORT , &GPIO_InitStructure);
	// G pin
	GPIO_InitStructure.GPIO_Pin = LED_B;
	GPIO_ResetBits( LED_B_PORT, LED_B );
	GPIO_Init( LED_B_PORT , &GPIO_InitStructure);
	// B pin
	GPIO_InitStructure.GPIO_Pin = LED_G;
	GPIO_ResetBits( LED_G_PORT, LED_G );
	GPIO_Init( LED_G_PORT , &GPIO_InitStructure);

	// RGB PWM TIMER SETUP
	//
	// Configure PWM modulation
	TIM_BaseInit.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_BaseInit.TIM_Period = 511;
	TIM_BaseInit.TIM_Prescaler = 0;
	TIM_BaseInit.TIM_ClockDivision = 1;
	TIM_BaseInit.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( LED_TIMER, &TIM_BaseInit );
	// Allow update buffered register content
	TIM_UpdateDisableConfig( LED_TIMER, DISABLE );
	// Setup Output Compare mode: PWM1, 
	// OCxREF set 1 on UEV, and cleared on OCE
	// Common configuration:
	TIM_OCStructInit(&TIM_OCInit);
	TIM_OCInit.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInit.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInit.TIM_OCNPolarity = TIM_OCNPolarity_Low;
	TIM_OCInit.TIM_Pulse = 255;
	TIM_OCInit.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM_OCInit.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	
	// Per CHANNEL configuration
	// LED_R --- CH2
	TIM_OCInit.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInit.TIM_OutputNState = TIM_OutputNState_Disable;

	TIM_OC2Init( LED_TIMER, &TIM_OCInit);
	TIM_OC2PreloadConfig( LED_TIMER, TIM_OCPreload_Enable);
	TIM_CCxCmd( LED_TIMER, TIM_Channel_2, TIM_CCx_Enable );
	
	// LED_G --- CH1
	TIM_OCInit.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInit.TIM_OutputNState = TIM_OutputNState_Disable;

	TIM_OC1Init( LED_TIMER, &TIM_OCInit);
	TIM_OC1PreloadConfig( LED_TIMER, TIM_OCPreload_Enable);
	TIM_CCxCmd( LED_TIMER, TIM_Channel_1, TIM_CCx_Enable );

	// LED_B --- CH3N
	TIM_OCInit.TIM_OutputState = TIM_OutputState_Disable;
	TIM_OCInit.TIM_OutputNState = TIM_OutputNState_Enable;

	TIM_OC3Init( LED_TIMER, &TIM_OCInit);
	TIM_OC3PreloadConfig( LED_TIMER, TIM_OCPreload_Enable);
	TIM_CCxCmd( LED_TIMER, TIM_Channel_3, TIM_CCx_Enable );

	// Global enable PWM
	TIM_CtrlPWMOutputs(LED_TIMER, ENABLE);

	//Enable timers
	TIM_Cmd( LED_TIMER, ENABLE);
}

/**
 * @brief Deconfigure RBB pwm modulation.
 * Deconfigure PWM modulation for RGB led. 
 **/
void led_rgb_deinit(void) 
{
	TIM_CtrlPWMOutputs(LED_TIMER, DISABLE);
	GPIO_SetBits( LED_CH_PORT, LED_CH1 | LED_CH2 | LED_CH3 | LED_CH4 );

	TIM_DeInit( LED_TIMER );

	//TODO DeInit GPIO
}

/**
 * @brief Brightness table.
 * Visible brightness is the nonlinear function of the PWM pulselength.
 **/
static const uint16_t led_rgb_brightness[] = { 0, 55, 223, 511 };

/**
 * @brief Set RGB led color.
 * Set RGB led color.
 * @param color New color.
 **/
void led_rgb_set_pwm(diskd_pixel_color_t color) 
{
	LED_R_TIMER_OCR = led_rgb_brightness[color.R];	
	LED_G_TIMER_OCR = led_rgb_brightness[color.G];	
	LED_B_TIMER_OCR = led_rgb_brightness[color.B];	
}

/**
 * @brief Pin array.
 **/
const static uint16_t led_rgb_pins[N_SECTORS] = { LED_CH1, LED_CH2, LED_CH3, LED_CH4 };

/**
 * @brief Select led sector.
 * Select one of led sectors.
 * @param sector sector number:
 **/
void led_rgb_set_sector(uint8_t sector)
{
	if (sector < N_SECTORS) {
		LED_CH_PORT->BSRR = LED_CH1 | LED_CH2 | LED_CH3 | LED_CH4;
		LED_CH_PORT->BRR = led_rgb_pins[sector];
	}
}

/**
 * @brief Select a group of led sectors.
 * Select a group of led sectors.
 * @param sector sectors binary mask.
 **/
void led_rgb_set_sectors( uint16_t enabled_sectors ) 
{
	uint16_t channels = 0;
	uint8_t i;

	for(i = 0; i < N_SECTORS; i++) 
		if (enabled_sectors & (1 << i)) channels |= led_rgb_pins[i];

	LED_CH_PORT->BSRR = LED_CH1 | LED_CH2 | LED_CH3 | LED_CH4;
	LED_CH_PORT->BRR = channels;
}
