#ifndef  DISK_DISPLAY_H
#define  DISK_DISPLAY_H

#include <stdint.h>

#include "bool.h"

/**
 * @brief Pixel color datatype.
 */
typedef union {
	struct {
		unsigned R:2;
		unsigned G:2;
		unsigned B:2;
	};
	uint8_t raw;
} diskd_pixel_color_t;

/**
 * @brief Frame buffer type.
 */
typedef diskd_pixel_color_t* diskd_framebuffer_t;

/**
 * @brief Colors constants.
 */
enum {
	DISKD_COLOR_BLACK = 0x00,
	DISKD_COLOR_RED = 0x03,
	DISKD_COLOR_GREEN = 0x0C,
	DISKD_COLOR_BLUE = 0x30,
	DISKD_COLOR_WHITE = 0x3f,
};

/**
 * @brief Frame dimentions and number of sectors.
 **/
typedef struct {
	uint16_t rows; /**< Number of rows. */
	uint16_t cols; /**< Total number of columns. */
	uint16_t frame_size; /**< Frame size. */
} diskd_frame_geometry_t;

/**
 * @brief Display parameters.
 * SVM amplitude calculated as
 * 
 *     mot_base_amp + mot_vel_amp * angular_velocity
 * 
 **/
typedef struct {
	_Accum mot_freq; /**< Disk rotation frequency, Hz. */
	_Fract mot_base_amp; /**< Base value of SVM signal amplitude. */
	_Fract mot_vel_amp; /**< Angular speed coefficent of  */
	_Fract mot_slide_delta; /**< Sliding mode delta. */

	_Fract mot_start_vel; /**< Inital angular velocity, Hz. */
	_Fract mot_start_accel; /**< Initial angular acceleration. */

	_Accum mot_max_accel; /**< Maximal allowed disk acceleration. */
	_Accum mot_max_accel_rate; /**< Maximal allowed disk acceleration growth rate. */
	_Accum mot_k_vel; /**< Velocity feedback coefficient. */
	_Accum mot_k_accel; /**< Accelaration feedback coefficient. */

	_Fract disp_delta; /**< Angle between INDEX sensor and first pixel.*/
	_Fract disp_lambda; /**< Display regulator coefficient. */
	diskd_pixel_color_t disp_forced_bright_color; /**< If nonblack, switch display in bright monchrome mode. */
} diskd_parameters_t;

/**
 * @brief Init Nipkow disk display parameters.
 * @param params Pointer to display parameters.
 **/
void diskd_parameters_init(diskd_parameters_t * params);

/**
 * @brief Init Nipkow disk display.
 * Init and start motor. Init led output and start displaying frame buffer.
 * @param params Display parameters.
 **/
void diskd_init(const diskd_parameters_t * params);

/**
 * @brief Get frame geometry.
 * Get frame geometry.
 * @param fg if @p fg is not NULL copy frame geometry.
 **/
const diskd_frame_geometry_t diskd_get_frame_geometry(diskd_frame_geometry_t * fg);

/**
 * @brief Get and lock frame back buffer. 
 * Display uses standard doublebuffering scheame. This function return
 * pointer to the backbuffer or NULL if it is already locked.
 *
 * @return Pointer to the backbuffer or NULL.
 **/
diskd_pixel_color_t * diskd_get_framebuffer(void);

/**
 * @brief Release locked backbuffer and swap the framebuffers.
 * Releases the backbuffer and swap it with current framebuffer or do nothing
 * if the backbuffer is not locked. This function may be locked until 
 * internal framebuffer is released by display routine.
 * @param wait If this parameter is TRUE wait until frame buffer and back buffer are swapped.
 **/
void diskd_release_framebuffer(BOOL wait);

/**
 * @brief Check if frame buffer and back buffer have been swapped.
 * Check if frame buffer and back buffer have been swapped.
 * @return TRUE if back buffer content changes was yanked in frame buffer.
 **/
BOOL diskd_framebuffer_updated(void);

/**
 * @brief Set endframe callback.
 * @a cb is pointer to callback which is called after back and front buffers are swapped.
 **/
void diskd_framebuffer_callback(void (*cb)());

/**
 * @brief Switch of display module.
 * Stop motors, turn off the leds.
 **/
void diskd_deinit();

#ifdef DEBUG
typedef struct {
	volatile BOOL updated;
	int32_t disp_err;
	_Accum disp_period;
	_Accum estim_vel;
	_Accum estim_accel;
	_Accum mot_accel;
	int32_t fp_ocr, i_icr;
} diskd_debug_t;

extern diskd_debug_t diskd_debug;
#endif

#endif  /*DISK_DISPLAY_H*/
