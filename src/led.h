#ifndef  LED_H
#define  LED_H

#define LED_PIN1 GPIO_Pin_4
#define LED_PIN2 GPIO_Pin_5
#define LED_PIN3 GPIO_Pin_6
#define LED_PINS (LED_PIN1 | LED_PIN2 | LED_PIN3)
#define LED_DELAY 10000000

void led_init(void);
void led_set(uint16_t pin, uint8_t value);
void led_blink(uint16_t pin); 


#endif  /*LED_H*/
