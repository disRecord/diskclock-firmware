#include "stm32f10x_conf.h"

#include "config.h"
#include "disk_display_config.h"

#include "bool.h"
#include "semaphore.h"
#include "led.h"
#include "led_rgb.h"
#include "ekf.h"
#include "motor.h"

#include "disk_display.h"

// Types of PendSV events
typedef enum {
	SPEED_ESTIMATOR = 0x01,
	BUFFERSWAP_CALLBACK = 0x02,
	NONE
} pend_sv_event_t;

static const diskd_frame_geometry_t frame_geometry = { 
	N_ROWS, // rows
	N_COLS, // cols 
	FRAME_BUFFER_SIZE, // rows*cols
};

// reason of PendSV interrupt
static pend_sv_event_t diskd_pendsv_event;

// allocate memory for buffers
static diskd_pixel_color_t buffer[2 * FRAME_BUFFER_SIZE];

// frmae and back buffers pointers
static diskd_pixel_color_t * frame_buffer = buffer;
static diskd_pixel_color_t * back_buffer = buffer + FRAME_BUFFER_SIZE;
// frame buffer syncronization 
semaphore_t back_buffer_lock = sem_unlocked;
volatile BOOL back_buffer_updated_flag = FALSE;

// configurable parameters
static diskd_parameters_t parameters;
static int32_t mot_stoped_threshold;

// frame buffers swap callback
void (*framebuffer_callback)() = 0;

#ifdef DEBUG
diskd_debug_t diskd_debug;
#endif

void diskd_parameters_init(diskd_parameters_t * params) 
{
	if (params == 0) return;

	params->mot_freq = 10.0k;
	params->mot_base_amp = 0.5r;
	params->mot_vel_amp = 0.015r;
	params->mot_slide_delta = 0.1r;

	params->mot_start_vel = 0.25k;
	params->mot_start_accel = 0.2k;

	params->mot_max_accel = 0.4k;
	params->mot_max_accel_rate = 0.1k;
	params->mot_k_vel = -2 * (-1);
	params->mot_k_accel = 2 * (-1)*(-1);

	params->disp_delta = 0.245r;
	params->disp_lambda = -0.7r;
	params->disp_forced_bright_color.raw = DISKD_COLOR_BLACK;
}


void diskd_init(const diskd_parameters_t * params)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_BaseInit;
	TIM_OCInitTypeDef TIM_OC;
	TIM_ICInitTypeDef TIM_IC;

	//TODO parameters sanity check
	parameters = *params;
	mot_stoped_threshold = (int32_t) DISKD_TIMER_TOP * (int32_t) (1.0k / parameters.mot_start_vel);

	// init submodules
	led_rgb_init();
	motor_init();

	// TIMER INTERRUPT 
	// set max priority
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = DISKD_IRQ_PRIORITY;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	// Velocity estimation routine 
	// Set PendSV priority to 1
	SCB->SHP[10] = (DISKD_IRQ_PRIORITY + 1) << 4; 

	// enable peripheral devices

	RCC_APB1PeriphClockCmd(DISKD_TIMER_APBPeriph, ENABLE);
	RCC_APB2PeriphClockCmd(INDEX_APBPeriph, ENABLE);

	// TIMER SETUP
	//
	RCC_ClocksTypeDef clock_freq;
	RCC_GetClocksFreq(&clock_freq);
	// init main timer TIM3
	// Period is fixed, but prescaler configured 
	// to confront specified frequency.
	TIM_BaseInit.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_BaseInit.TIM_Period = DISKD_TIMER_TOP;
	TIM_BaseInit.TIM_Prescaler = (clock_freq.PCLK1_Frequency / DISKD_TIMER_TOP) >> DISKD_TIMER_FREQ_2POWER;
	TIM_BaseInit.TIM_ClockDivision = 0;
	TIM_BaseInit.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( DISKD_TIMER, &TIM_BaseInit );
	// Set TOP value bufferization.
	TIM_ARRPreloadConfig( DISKD_TIMER, ENABLE);
	// Allow update buffered register content
	TIM_UpdateDisableConfig( DISKD_TIMER, DISABLE );
	//
	// Configure INDEX capture pin
	GPIO_InitStructure.GPIO_Pin = INDEX_S;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( INDEX_PORT , &GPIO_InitStructure);
	
	// CHANNEL 1 --- INDEX signal capture
	//TIM_ICStructInit(&TIM_ICInit);
	TIM_IC.TIM_Channel = TIM_Channel_1;
	TIM_IC.TIM_ICPolarity = TIM_ICPolarity_Rising;
	TIM_IC.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_IC.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_IC.TIM_ICFilter = 0x0003;
	TIM_ICInit( DISKD_TIMER, &TIM_IC );

	// Enable INDEX capture interrupt
	TIM_ClearITPendingBit(DISKD_TIMER, DISKD_TIMER_SIG_INDEX);
	TIM_ITConfig( DISKD_TIMER, DISKD_TIMER_SIG_INDEX , ENABLE );

	// Enable update interrupt for overflow detection
	TIM_ClearITPendingBit(DISKD_TIMER, TIM_IT_Update);
	TIM_ITConfig( DISKD_TIMER, TIM_IT_Update , ENABLE );
	
	// Configure motor control interrupt
	// CHANNEL 2 --- motor control
	TIM_OCStructInit(&TIM_OC);
	TIM_OC.TIM_OCMode = TIM_OCMode_Timing;
	TIM_OC.TIM_Pulse = 0;
	TIM_OC2Init( DISKD_TIMER, &TIM_OC );
	TIM_OC2PreloadConfig( DISKD_TIMER, TIM_OCPreload_Disable);
	TIM_CCxCmd( DISKD_TIMER, TIM_Channel_2, TIM_CCx_Enable );

	// Enable motor interrupt
	TIM_ClearITPendingBit(DISKD_TIMER, DISKD_TIMER_SIG_MOTOR);
	TIM_ITConfig( DISKD_TIMER, DISKD_TIMER_SIG_MOTOR, ENABLE );

	TIM_Cmd( DISKD_TIMER, ENABLE);

	// Configure channel switch interrupt
	// CHANNEL 3 --- led display control
	// Setup Output Compare mode
	TIM_OCStructInit(&TIM_OC);
	TIM_OC.TIM_OCMode = TIM_OCMode_Timing;
	TIM_OC.TIM_Pulse = 0;
	TIM_OC3Init( DISKD_TIMER, &TIM_OC);
	TIM_OC3PreloadConfig( DISKD_TIMER, TIM_OCPreload_Disable);
	TIM_CCxCmd( DISKD_TIMER, TIM_Channel_3, TIM_CCx_Enable );

	// Enable led display interrupt.
	TIM_ITConfig( DISKD_TIMER, DISKD_TIMER_SIG_DISPLAY , ENABLE );

	TIM_Cmd(DISKD_TIMER, ENABLE);;
	motor_enable();
}


// variables shared beetween interrupt handlers
// led display state
static volatile int32_t disp_first_pixel_ocr_value;
static volatile _Accum disp_period = DISKD_TIMER_TOP / FRAME_BUFFER_SIZE;
// index signal data: velocity and acceleration estimation
static volatile int32_t index_measured_period;
static volatile int32_t index_icr_value;
// motor control
static volatile ekf_state_t mot_state = { 0.25k, 0.1k };
static volatile _Accum mot_amplitude = 1.0k;

void TIM3_IRQHandler(void)
{
	// led display state
	static int16_t sector = 0;
	static int16_t sector_column = N_COLS_PER_SECTOR - 1;
	static int16_t row = 0;
	static int16_t pixel_index = 0;
	static unsigned _Accum disp_ocr_value;
	// velocity and acceleration estimation
	static BOOL index_first_sig = TRUE;
	// motor control
	static uint16_t mot_phase;
	static unsigned _Accum mot_ocr_value;


	// LED DISPLAY EVENT
	if (TIM_GetITStatus(DISKD_TIMER, DISKD_TIMER_SIG_DISPLAY)) {
		// Clear pending flag.
		TIM_ClearITPendingBit(DISKD_TIMER, DISKD_TIMER_SIG_DISPLAY);

		// there are two display modes: bright monocrome and full color mode
		if (parameters.disp_forced_bright_color.raw != DISKD_COLOR_BLACK && sector == 0) {
			// bright color mode: light pixels in all sectors
			// load new OCR value and skip all sectors 
			disp_ocr_value += disp_period*N_SECTORS;
			DISKD_TIMER_SIG_DISPLAY_OCR = disp_ocr_value;

			// now light pixels in all sectros: current pixel index points on pixel in first sector
			uint8_t enabled_sectors = 0;
			for(sector = 0; sector < N_SECTORS; sector++) {
				if (frame_buffer[pixel_index].raw != DISKD_COLOR_BLACK) enabled_sectors |= 1 << sector;
				pixel_index -= SECTOR_SIZE + 1;
				if (row == sector) pixel_index += N_ROWS;
			}
			led_rgb_set_pwm( parameters.disp_forced_bright_color );
			led_rgb_set_sectors( enabled_sectors );

			// skip sector iteration
			sector = N_SECTORS;
		}
		else {
			// full color mode: light pixels in each sector independently
			
			// load next OCR value
			disp_ocr_value += disp_period;
			DISKD_TIMER_SIG_DISPLAY_OCR = disp_ocr_value;
			// display pixel 
			led_rgb_set_pwm( frame_buffer[ pixel_index ] );
			led_rgb_set_sector( sector );
			// switch to next sector
			sector++;
		}
		// prepare for next call
		// move cursor in frame
		if (sector == N_SECTORS) {
			sector = 0;

			if (sector_column != 0) {
				sector_column--;
			}
			else {
				sector_column = N_COLS_PER_SECTOR - 1;

				row++;
				if (row == N_ROWS) {
					row = 0;

					disp_first_pixel_ocr_value = DISKD_TIMER_SIG_DISPLAY_OCR;
					disp_ocr_value = DISKD_TIMER_SIG_DISPLAY_OCR;
					
					// swap buffers if lock is free
					if (sem_try_lock(&back_buffer_lock)) {
						diskd_pixel_color_t * tmp;

						tmp = back_buffer;
						frame_buffer = back_buffer;
						back_buffer = tmp;
						back_buffer_updated_flag = FALSE;

						sem_free(&back_buffer_lock);

						// Call user callbcak in lower priority code.
						// Set PendSV exception pending bit.
						diskd_pendsv_event = BUFFERSWAP_CALLBACK;
						SCB->ICSR = SCB_ICSR_PENDSVSET_Msk;
					}
				}
			}
		}
		pixel_index = row - sector;
		if (pixel_index < 0) pixel_index += N_ROWS;
		pixel_index += (N_SECTORS-1 - sector)*SECTOR_SIZE + sector_column*N_ROWS;
	}

	// MOTOR EVENT
	if (TIM_GetITStatus(DISKD_TIMER, DISKD_TIMER_SIG_MOTOR)) {
		// Clear pending flag.
		TIM_ClearITPendingBit(DISKD_TIMER, DISKD_TIMER_SIG_MOTOR);

		_Accum mot_period;

		// set SVM
		motor_set_svm(mot_amplitude, mot_phase);
		// select next phase
		mot_phase++;
		if (mot_phase == MOT_N_SVM_PHASES) {
			mot_phase = 0;
			mot_ocr_value = DISKD_TIMER_SIG_MOTOR_OCR;
		}
		// setup next interrupt time
		mot_period = DISKD_TIMER_TOP / (mot_state.vel * MOT_N_PHASES);
		mot_ocr_value += mot_period;
		DISKD_TIMER_SIG_MOTOR_OCR = mot_ocr_value;
		mot_state.vel += mot_state.accel * (mot_period >> DISKD_TIMER_TOP_2POWER);
	}

	// handle INDEX event: estimate angular velocity
	if (TIM_GetITStatus(DISKD_TIMER, DISKD_TIMER_SIG_INDEX)) {
		TIM_ClearITPendingBit(DISKD_TIMER, DISKD_TIMER_SIG_INDEX);

		if (index_first_sig) {
			index_icr_value = DISKD_TIMER_SIG_INDEX_ICR;
			if (TIM_GetITStatus(DISKD_TIMER, TIM_IT_Update) && index_icr_value > DISKD_TIMER_TOP/2) {
				index_icr_value -= DISKD_TIMER_TOP+1;
				TIM_ClearITPendingBit(DISKD_TIMER, TIM_IT_Update);
			}
			index_first_sig = FALSE;
		}
		else {
			// PERIOD MEASUREMENT
			index_measured_period = DISKD_TIMER_SIG_INDEX_ICR;
			if (TIM_GetITStatus(DISKD_TIMER, TIM_IT_Update) && index_measured_period <  DISKD_TIMER_TOP/2) {
				index_icr_value -= DISKD_TIMER_TOP+1;
				TIM_ClearITPendingBit(DISKD_TIMER, TIM_IT_Update);
			}
			index_measured_period -= index_icr_value;
			index_icr_value = DISKD_TIMER_SIG_INDEX_ICR;
		
			// Call velocity estimation code with lower priority:
			// set PendSV exception pending bit.
			diskd_pendsv_event = SPEED_ESTIMATOR;
			SCB->ICSR = SCB_ICSR_PENDSVSET_Msk;
		}
	}

	// TIMER OVERFLOW handle for INDEX event
	if (TIM_GetITStatus(DISKD_TIMER, TIM_IT_Update)) {
		index_icr_value -= DISKD_TIMER_TOP+1;

		if ( index_icr_value < -mot_stoped_threshold ) {
			// We have not received any INDEX signal for a long time, set motor speed to inital values.
			mot_state.vel = parameters.mot_start_vel;
			mot_state.accel = parameters.mot_start_accel;
		}

		TIM_ClearITPendingBit(DISKD_TIMER, TIM_IT_Update);
	}
}

// ANGULAR SPEED ESTIMATION
// TODO atomic operations 
// (However, 32bit assigment must be atomic so it is already woking fine enough).
void diskd_speed_estimator() 
{
	static ekf_state_t estim;

	if (index_measured_period <= DISKD_TIMER_TOP) {
		// angular velocity is high enough, EKF can work properly
		ekf_filter(&estim, index_measured_period);	
	}
	else {
		estim = mot_state;
	}

	if (index_measured_period <= DISKD_TIMER_TOP/3) {
		// EKF is working long enough, use its estimation
		_Accum accel_delta;

		// calculate accel increment for motor control
		// modify estim to imitate 
		accel_delta =  (parameters.mot_k_vel*(parameters.mot_freq - estim.vel) - parameters.mot_k_accel*estim.accel) / estim.vel;
		// check growth rate
		if (accel_delta > parameters.mot_max_accel_rate) accel_delta = parameters.mot_max_accel_rate;
		estim.accel += accel_delta;
		// check upper limit
		if (estim.accel > parameters.mot_max_accel) estim.accel = parameters.mot_max_accel;

		mot_state = estim;
	}

	// Calculate SVM amplitude: second term represents contr-emf correction, higher speed causes higher contr-emf values.
	_Accum max_amp = parameters.mot_base_amp + parameters.mot_vel_amp * mot_state.vel;
	// Reduce motor current when velocity is stable.
	if (mot_state.vel > parameters.mot_freq - 0.05k) {
		max_amp -= parameters.mot_slide_delta;

		led_set(LED_PIN1, TRUE);
	}
	mot_amplitude = max_amp;

	// Display position regulator
	int32_t tmp;
	tmp = disp_first_pixel_ocr_value;
	if (tmp >= index_icr_value) tmp -= DISKD_TIMER_TOP+1;
	_Accum corr = tmp - index_icr_value + ( ((1024 - ((_Accum) parameters.disp_delta << 10))*index_measured_period) >> 10 );

#ifdef DEBUG
	diskd_debug.disp_err = corr;
#endif

	corr = - (1.0k + parameters.disp_lambda)* corr / FRAME_BUFFER_SIZE;
	disp_period = DISKD_TIMER_TOP/(estim.vel*FRAME_BUFFER_SIZE);
	if ( corr < -(disp_period >> 1) ) corr = -(disp_period >> 1);
	if ( corr > (disp_period << 1)) corr = (disp_period << 1);
	disp_period += corr;

#ifdef DEBUG
	diskd_debug.fp_ocr = disp_first_pixel_ocr_value;
	diskd_debug.i_icr = index_icr_value;
	diskd_debug.estim_vel = estim.vel;
	diskd_debug.estim_accel = estim.accel;
	diskd_debug.mot_accel = mot_state.accel;
	diskd_debug.disp_period = disp_period;
	diskd_debug.updated = TRUE;
#endif
}

// lower priority code
void PendSV_Handler(void)
{
	// reset interrupt flag
	SCB->ICSR = SCB_ICSR_PENDSVCLR_Msk;
	
	// check reason of interrupt
	// INDEX event
	if	(diskd_pendsv_event & SPEED_ESTIMATOR) {
		diskd_speed_estimator();
		diskd_pendsv_event &= ~SPEED_ESTIMATOR;
	}
	
	// FRAMEBUFFER SWAP event
	if	(diskd_pendsv_event & BUFFERSWAP_CALLBACK) {
		void (*callback)(void) = framebuffer_callback; // atomic opration
		if (callback) callback();
		diskd_pendsv_event &= ~BUFFERSWAP_CALLBACK;
	}
}


const diskd_frame_geometry_t diskd_get_frame_geometry(diskd_frame_geometry_t * fg) 
{
	if (fg != 0) *fg = frame_geometry;
	return frame_geometry;
}

diskd_pixel_color_t * diskd_get_framebuffer(void) 
{
	if (sem_try_lock(&back_buffer_lock)) {
		back_buffer_updated_flag = TRUE;
		return back_buffer;
	}
	else {
		return 0;
	}
}

BOOL diskd_framebuffer_updated(void)
{
	return ! back_buffer_updated_flag;
}

void diskd_release_framebuffer(BOOL wait)
{
	sem_free(&back_buffer_lock);
	// TODO interrupt is running check
	if (wait) {
		// wait until flag is down
		while (back_buffer_updated_flag) {}
	}
}

void diskd_framebuffer_callback(void (*cb)()) 
{
	framebuffer_callback = cb;
}

void diskd_deinit() 
{
	TIM_Cmd(DISKD_TIMER, DISABLE);
	TIM_DeInit(DISKD_TIMER);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = INDEX_S;
	GPIO_Init( INDEX_PORT , &GPIO_InitStructure);

	// deinit submodules
	led_rgb_deinit();
	motor_deinit();
}
