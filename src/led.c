#include "stm32f10x_conf.h"

#include "led.h"

void led_init(void) 
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = LED_PINS;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init( GPIOA , &GPIO_InitStructure);
}

void led_set(uint16_t pin, uint8_t value) 
{
	if (pin & LED_PINS) GPIO_WriteBit(GPIOA, pin, value);
}

void my_wait(uint32_t delay) {
	while (delay) delay--;
}

void led_blink(uint16_t pin) 
{
	//uint32_t delay;

	if (pin & LED_PINS) {
		//for (delay = LED_DELAY; delay != 0; delay--) {}
		my_wait(LED_DELAY);
		GPIO_SetBits(GPIOA, pin);
		//for (delay = LED_DELAY; delay != 0; delay--) {}
		my_wait(LED_DELAY);
		GPIO_ResetBits(GPIOA, pin);
	}
}

