#include <stdio.h>

#include "stm32f10x_conf.h"

#include "config.h"
#include "disk_display_config.h"

#include "led.h"
#include "uart1.h"
#include "ekf.h"

void diskd_init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_BaseInit;
	TIM_ICInitTypeDef TIM_IC;

	// TIMER INTERRUPT 
	// set max priority
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = DISKD_IRQ_PRIORITY;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// enable peripheral devices

	RCC_APB1PeriphClockCmd(DISKD_TIMER_APBPeriph, ENABLE);
	RCC_APB2PeriphClockCmd(INDEX_APBPeriph, ENABLE);

	// TIMER SETUP
	//
	RCC_ClocksTypeDef clock_freq;
	RCC_GetClocksFreq(&clock_freq);
	// init main timer TIM3
	// Period is fixed, but prescaler configured 
	// to confront specified frequency.
	TIM_BaseInit.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_BaseInit.TIM_Period = DISKD_TIMER_TOP;
	TIM_BaseInit.TIM_Prescaler = (clock_freq.PCLK1_Frequency / DISKD_TIMER_TOP) >> DISKD_TIMER_FREQ_2POWER;
	TIM_BaseInit.TIM_ClockDivision = 0;
	TIM_BaseInit.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( DISKD_TIMER, &TIM_BaseInit );
	// Set TOP value bufferization.
	TIM_ARRPreloadConfig( DISKD_TIMER, ENABLE);
	// Allow update buffered register content
	TIM_UpdateDisableConfig( DISKD_TIMER, DISABLE );
	//
	// Configure INDEX capture pin
	GPIO_InitStructure.GPIO_Pin = INDEX_S;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( INDEX_PORT , &GPIO_InitStructure);
	
	// CHANNEL 1 --- INDEX signal capture
	//TIM_ICStructInit(&TIM_ICInit);
	TIM_IC.TIM_Channel = TIM_Channel_1;
	TIM_IC.TIM_ICPolarity = TIM_ICPolarity_Rising;
	TIM_IC.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_IC.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_IC.TIM_ICFilter = 0x000F;
	TIM_ICInit( DISKD_TIMER, &TIM_IC );

	// Enable INDEX capture interrupt
	TIM_ClearITPendingBit(DISKD_TIMER, DISKD_TIMER_SIG_INDEX);
	TIM_ITConfig( DISKD_TIMER, DISKD_TIMER_SIG_INDEX , ENABLE );

	// Enable update interrupt for overflow detection
	TIM_ClearITPendingBit(DISKD_TIMER, TIM_IT_Update);
	TIM_ITConfig( DISKD_TIMER, TIM_IT_Update , ENABLE );

	TIM_Cmd( DISKD_TIMER, ENABLE);
}

void TIM3_IRQHandler(void)
{
	static int8_t led;
	static ekf_state_t estim = { 1k, 1k };
	static int32_t prev_index_icr;

	// handle INDEX event: estimate angular velocity
	if (TIM_GetITStatus(DISKD_TIMER, DISKD_TIMER_SIG_INDEX)) {
		int32_t T;

		T = DISKD_TIMER_SIG_INDEX_ICR;
		if (TIM_GetITStatus(DISKD_TIMER, TIM_IT_Update) && T < DISKD_TIMER_TOP/2) {
			// Timer update event has not ben handled properly yet.
			T += DISKD_TIMER_TOP+1;
			TIM_ClearITPendingBit(DISKD_TIMER, TIM_IT_Update);
		}
		T -= prev_index_icr;
		prev_index_icr = DISKD_TIMER_SIG_INDEX_ICR;

		assert_param(T >= 0);
		
		if (T <= DISKD_TIMER_TOP) {
			// angular velocity is high enough, EKF can work properly
			ekf_filter(&estim, T);	
		}
		else {
			// velocity is low, fall back to inital conditions
			// TODO: estimation fo low velocities
			estim.vel = 1k;
			estim.accel = 1k;
		}
		TIM_ClearITPendingBit(DISKD_TIMER, DISKD_TIMER_SIG_INDEX);

		printf("vel = %f\n", (float) (estim.vel << DISKD_TIMER_FREQ_2POWER));
		led_set(LED_PIN1, led);
		led = !led;
	}

	if (TIM_GetITStatus(DISKD_TIMER, TIM_IT_Update)) {
		prev_index_icr -= DISKD_TIMER_TOP+1;
		TIM_ClearITPendingBit(DISKD_TIMER, TIM_IT_Update);
	}

}


int main()
{
	led_init();
	uart1_init();
	diskd_init();

	for(;;) {}
	
	return 0;
}

void assert_failed(uint8_t* file, uint32_t line) {
	dprintf(fileno(stderr), "Assertion failed in file \"%s\", line %lu.\n", file, line);
	fflush(stderr);
}
