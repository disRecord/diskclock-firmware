#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stm32f10x_conf.h"

#include "uart1.h"
#include "led.h"
#include "disk_display.h"


int main()
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	led_init();
	uart1_init();	

	{
		diskd_parameters_t params;
		float tmp;
		unsigned int ui_tmp;

		diskd_parameters_init(&params);

		puts("\n\nStart disp test.");

		printf("Enter mot_vel: ");
		scanf("%f", &tmp);
		params.mot_freq = tmp;

		printf("Enter disp_delta: ");
		scanf("%f", &tmp);
		params.disp_delta = tmp;

		printf("Set display force color: ");
		scanf("%x", &ui_tmp);
		params.disp_forced_bright_color.raw = ui_tmp;

		diskd_init(&params);
	}

	diskd_framebuffer_t buffer;
	/*diskd_pixel_color_t * ptr;*/
	diskd_frame_geometry_t geom;
	uint16_t i, j;

	diskd_get_frame_geometry(&geom);
	buffer = diskd_get_framebuffer();

	memset(buffer, 0, geom.frame_size * sizeof(diskd_pixel_color_t));

	j = geom.frame_size - geom.rows;
	for(i = 0; i < geom.rows; i++) {
		buffer[i].raw = DISKD_COLOR_BLUE;
		buffer[i + geom.rows*5].raw = DISKD_COLOR_RED;
		buffer[i + geom.rows*15].raw = DISKD_COLOR_RED;
		buffer[i + geom.rows*25].raw = DISKD_COLOR_RED;
		buffer[i + geom.rows*35].raw = DISKD_COLOR_RED;
		buffer[i + j].raw = DISKD_COLOR_BLUE;
	}
	for(j = 0; j < geom.cols; j++) {
		buffer[j*geom.rows].raw = DISKD_COLOR_BLUE;
		buffer[j*geom.rows + 4].raw = DISKD_COLOR_GREEN;
		buffer[j*geom.rows + geom.rows - 1].raw = DISKD_COLOR_BLUE;
	}

	diskd_release_framebuffer(FALSE);

	for(;;) {
		while (!diskd_debug.updated) {}
		printf("vel_e = %f, err = %ld, period = %f, fp_ocr = %ld, index_icr = %ld\n", (float) diskd_debug.estim_vel, diskd_debug.disp_err, (float) diskd_debug.disp_period , diskd_debug.fp_ocr, diskd_debug.i_icr );
		diskd_debug.updated = FALSE;
	}

	return 0;
}

void assert_failed(uint8_t* file, uint32_t line) {
	dprintf(fileno(stderr), "Assertion failed in file \"%s\", line %lu.\n", file, line);
	fflush(stderr);
}
