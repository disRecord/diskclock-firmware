#include "stm32f10x_conf.h"

#include "config.h"
#include "backup.h"

/**
 * @brief Init backup module.
 **/
void backup_init(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
}

/**
 * @brief Read data from backup area.
 * Copy data from backup memory area to @p dst area. 
 * @param dst pointer to recever buffer
 * @param src_reg first backup register offset, total number of used registers is @p (size+1)/2.
 * @param size receiver buffer size.
 **/
void backup_read(void * _dst, uint16_t src_reg, int size)
{

	uint8_t * dst = (uint8_t *) _dst;
	uint16_t tmp;
	uint16_t i = 0;

	while (i < size) {
		tmp = BKP_ReadBackupRegister(src_reg);
		src_reg += 4;

		dst[i++] = (uint8_t) tmp;
		if (i < size)	dst[i++] = (uint8_t) (tmp >> 8);
	}
}

/**
 * @brief Write data to backup area.
 * Copy data to backup memory area from $p src area. 
 * @param src pointer to data buffer.
 * @param dst_reg first backup register offset, total number of used registers is @p (size+1)/2.
 * @param size buffer size.
 **/
void backup_write(uint16_t dst_reg, void * _src, int size)
{

	uint8_t * src = (uint8_t *) _src;
	uint16_t tmp;
	uint16_t i = 0;

	//PWR_BackupAccessCmd(ENABLE);
	PWR->CR |= PWR_CR_DBP;
	while (i < size) {
		tmp = src[i++];
		if (i < size) tmp |= (uint16_t) src[i++] << 8;

		BKP_WriteBackupRegister(dst_reg, tmp);
		dst_reg += 4;
	}
	//PWR_BackupAccessCmd(DISABLE);
	PWR->CR &= ~PWR_CR_DBP;
}

/**
 * @brief Set backup area consistent flag.
 **/
void backup_set_consistent(void) 
{
	//PWR_BackupAccessCmd(ENABLE);
	PWR->CR |= PWR_CR_DBP;
	BKP_WriteBackupRegister(BACKUP_CHECK_REG, BACKUP_CHECK_VALUE);
	//PWR_BackupAccessCmd(DISABLE);
	PWR->CR &= ~PWR_CR_DBP;
}

