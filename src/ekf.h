#ifndef  EKF_H
#define  EKF_H

/**
 * @brief Motor state vector type.
 **/
typedef struct {
	_Accum vel; /**< Angular velocity, revolutions per cycle timer period. */
	_Accum accel; /**<  Angular acceleration.*/
} ekf_state_t;


ekf_state_t ekf_filter(ekf_state_t * prev_estimate, _Accum T);

#endif  /*EKF_H*/
