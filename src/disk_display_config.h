#ifndef  DISK_DISPLAY_CONFIG_H
#define  DISK_DISPLAY_CONFIG_H

#include "stm32f10x_conf.h"

// Setup display geometry.
#define N_SECTORS				4
#define N_ROWS					10
#define N_COLS_PER_SECTOR		10

#define	N_COLS					(N_SECTORS*N_COLS_PER_SECTOR)
#define SECTOR_SIZE				(N_ROWS*N_COLS_PER_SECTOR)
#define FRAME_BUFFER_SIZE				(SECTOR_SIZE*N_SECTORS)

// Syncronization regulator parameter
#define LAMBDA				-0.9
#define DISPLAY_START_ANGLE  160

// Motor configuration

#define DISKD_TIMER				TIM3
#define DISKD_TIMER_APBPeriph	RCC_APB1Periph_TIM3
//
// TIMER channels:
// CH1 --- Index signal capture
// CH2 --- Motor control
// CH3 --- LED control
// !!!! MODIFY diskd_init() diskd_deinit()

#define DISKD_TIMER_SIG_INDEX			TIM_IT_CC1
#define DISKD_TIMER_SIG_INDEX_ICR		(DISKD_TIMER->CCR1)

#define DISKD_TIMER_SIG_MOTOR			TIM_IT_CC2
#define DISKD_TIMER_SIG_MOTOR_OCR		(DISKD_TIMER->CCR2)

#define DISKD_TIMER_SIG_DISPLAY			TIM_IT_CC3
#define DISKD_TIMER_SIG_DISPLAY_OCR		(DISKD_TIMER->CCR3)

#define DISKD_TIMER_TOP					0xffff
// DISKD_TIMER_TOP = (1 << DISKD_TIMER_TOP_2POWER) - 1
#define DISKD_TIMER_TOP_2POWER			16
// the base frequency of timer is (1 << DISKD_TIMER_FREQ_2POWER)
#define DISKD_TIMER_FREQ_2POWER			0


// INDEX INPUT

#define INDEX_PORT			GPIOA
#define INDEX_APBPeriph		RCC_APB2Periph_GPIOA
#define INDEX_S				GPIO_Pin_6

// MOTOR OUTPUT

#define MOT_N_SVM_PHASES		24
#define MOT_N_PHASES			(4*MOT_N_SVM_PHASES)
// Approx 36 kHz PWM modulation
#define MOT_TIMER_PERIOD			1024
#define MOT_TIMER_PERIOD_2POWER		10

#define MOT_APB2Periph		(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOC|RCC_APB2Periph_AFIO)
#define MOT_APB1Periph		RCC_APB1Periph_TIM4
// ABC signal pins
#define MOT_PORT			GPIOB
#define MOT_A				GPIO_Pin_9 
#define MOT_B				GPIO_Pin_8
#define MOT_C				GPIO_Pin_7

#define MOT_TIMER			TIM4
#define MOT_A_TIMER_OCR		(TIM4->CCR4)
#define MOT_B_TIMER_OCR		(TIM4->CCR3)
#define MOT_C_TIMER_OCR		(TIM4->CCR2)

// EN signal
#define MOT_EN_PORT			GPIOC
#define MOT_EN				GPIO_Pin_3

// LED MODULE

#define LED_APB2Periph		(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOC|RCC_APB2Periph_AFIO|RCC_APB2Periph_TIM8)
#define LED_APB1Periph		0

// channel selection
#define LED_CH_PORT				GPIOB
#define LED_CH1					GPIO_Pin_6
#define LED_CH2					GPIO_Pin_12
#define LED_CH3					GPIO_Pin_14
#define LED_CH4					GPIO_Pin_10


// LED RGB PWM

// RGB outputs
#define LED_R_PORT			GPIOC
#define LED_R				GPIO_Pin_7
#define LED_G_PORT			GPIOC
#define LED_G				GPIO_Pin_6
#define LED_B_PORT			GPIOB
#define LED_B				GPIO_Pin_1

// RGB timers
#define LED_TIMER				TIM8
#define LED_R_TIMER_OCR			(TIM8->CCR2)
#define LED_G_TIMER_OCR			(TIM8->CCR1)
#define LED_B_TIMER_OCR			(TIM8->CCR3) // output on TIM8_CH3N

#endif  /*DISK_DISPLAY_CONFIG_H*/
