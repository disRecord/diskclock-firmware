#include "stm32f10x_conf.h"

#include "led_rgb.h"
#include "led.h"

uint16_t led_pin = GPIO_Pin_4;

void TIM1_UP_IRQHandler(void) {
	static int8_t color = 0;
	static int8_t channel = 0;
	static int8_t led = 0;

	if ( TIM_GetITStatus(TIM1, TIM_IT_Update) ) {
		led_set( LED_PIN1, led);
		led = !led;

		diskd_pixel_color_t c;
		c.R = color & 0x7; c.G = (color >> 2) & 0x7; c.B = (color >> 4) & 0x7;
		led_rgb_set_pwm( c );
		led_rgb_set_sector( channel );
		color++;
		channel++;
		if (channel == 4) channel = 0;

		TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
	}
}

int main() 
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);


	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_TimeBaseInitTypeDef TIM_BaseInitStruct;
	TIM_BaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_BaseInitStruct.TIM_Period = 10000;
	TIM_BaseInitStruct.TIM_Prescaler = 7200;
	TIM_BaseInitStruct.TIM_ClockDivision = 0;
	TIM_BaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( TIM1, &TIM_BaseInitStruct );
	TIM_UpdateDisableConfig(TIM1, DISABLE);

	TIM_ClearITPendingBit( TIM1, TIM_IT_Update );
	TIM_ITConfig( TIM1, TIM_IT_Update , ENABLE );

	led_init();
	led_rgb_init();
	led_rgb_set_sector( 0 );
	diskd_pixel_color_t c;
	c.raw = DISKD_COLOR_BLUE;
	led_rgb_set_pwm( c );

//	TIM_Cmd(TIM1, ENABLE);

	for(;;) { }
}


void assert_failed(uint8_t* file, uint32_t line) {
	for(;;) {}
}
