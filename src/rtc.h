#ifndef  RTC_H
#define  RTC_H

#include <sys/time.h>

int rtc_init(void);

int settimeofday(const struct timeval *tv, const struct timezone *tz);

#endif  /*RTC_H*/
