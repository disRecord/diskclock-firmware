#include <sys/time.h>
#include <time.h>
#include <string.h>
#include <stdio.h>

#include "stm32f10x_conf.h"

#include "led.h"
#include "uart1.h"
#include "rtc.h"

void TIM1_UP_IRQHandler(void) {
	static int8_t led = 0;

	if ( TIM_GetITStatus(TIM1, TIM_IT_Update) ) {
		char * time_date_srt;
		char time_str[9];
		time_t tval;

		led_set( LED_PIN1, led);
		led = !led;

		time(&tval);
		time_date_srt = ctime(&tval);
		strncpy(time_str, time_date_srt + 11, 8);
		printf("Current time: %s.\n", time_str);

		TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
	}
}

void clock_set_time(int h, int m, int s) {
	struct timeval tv;
	struct tm ts;

	ts.tm_sec = s;
	ts.tm_min = m;
	ts.tm_hour = h;
	ts.tm_mday = 0;
	ts.tm_mon = 0;
	ts.tm_year = 2000;
	ts.tm_isdst = 0;

	tv.tv_sec = mktime(&ts);
	tv.tv_usec = 0;

	settimeofday(&tv, 0);
}


int main() 
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_TimeBaseInitTypeDef TIM_BaseInitStruct;
	TIM_BaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_BaseInitStruct.TIM_Period = 10000;
	TIM_BaseInitStruct.TIM_Prescaler = 7200;
	TIM_BaseInitStruct.TIM_ClockDivision = 0;
	TIM_BaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( TIM1, &TIM_BaseInitStruct );
	TIM_UpdateDisableConfig(TIM1, DISABLE);
	TIM_ARRPreloadConfig( TIM1, ENABLE );

	TIM_ClearITPendingBit( TIM1, TIM_IT_Update );
	TIM_ITConfig( TIM1, TIM_IT_Update , ENABLE );

	uart1_init();
	if (rtc_init()) {
		int h, m, s;
		puts("Enter current time, please (HH MM SS): ");
		scanf("%d %d %d", &h, &m, &s);
		clock_set_time(h, m, s);
	}	

	TIM_Cmd(TIM1, ENABLE);

	for(;;) { }
}

void assert_failed(uint8_t* file, uint32_t line) {
	for(;;) {}
}
