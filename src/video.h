#ifndef  VIDEO_H
#define  VIDEO_H

#include "disk_display.h"

/**
 * @brief Video store mode: by rows or by columns.
 **/
typedef enum {
	ROWS, /**< Video raw data is sequence of image rows. */
	COLS, /**< Video raw data is sequence of image columns. */
} video_store_mode_t;

typedef struct {
	int data_size;  /**< Overral size in pixels. */
	int frame_shift; /**< Shift of one frame on pixels. */
	video_store_mode_t store_mode; /**< Store mode. */
	const diskd_pixel_color_t * data; /**< Raw video data. */
} video_t;

void video_init(const video_t * video);

void video_deinit(void);

//TODO Cyclic/single play. End video callback.

#endif  /*VIDEO_H*/
