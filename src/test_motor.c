#include "stm32f10x_conf.h"

#include "led.h"
#include "disk_display_config.h"
#include "motor.h"


void TIM1_UP_IRQHandler(void) {
	static int8_t phase = 0;
	static int8_t led = 0;
	static uint16_t cycle = 0;
	static uint16_t freq = 3;

	if ( TIM_GetITStatus(TIM1, TIM_IT_Update) ) {
		motor_set_svm( 1000, phase );
		phase++;
		if (phase == MOT_N_SVM_PHASES) {
			phase = 0;
			led_set( LED_PIN1, led);
			led = !led;

			cycle++;
			if ((cycle % (2*2*freq)) == 0) {
				cycle = 0;
				if (freq < 30) freq = freq+1;
				TIM_SetAutoreload(TIM1, 10000 / (MOT_N_PHASES*freq));
				}
		}

		TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
	}
}

int main() 
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);


	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_TimeBaseInitTypeDef TIM_BaseInitStruct;
	TIM_BaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_BaseInitStruct.TIM_Period = 10000 / (MOT_N_PHASES);
	TIM_BaseInitStruct.TIM_Prescaler = 7200;
	TIM_BaseInitStruct.TIM_ClockDivision = 0;
	TIM_BaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( TIM1, &TIM_BaseInitStruct );
	TIM_UpdateDisableConfig(TIM1, DISABLE);
	TIM_ARRPreloadConfig( TIM1, ENABLE );

	TIM_ClearITPendingBit( TIM1, TIM_IT_Update );
	TIM_ITConfig( TIM1, TIM_IT_Update , ENABLE );

	led_init();
	motor_init();
	motor_enable();

	TIM_Cmd(TIM1, ENABLE);

	for(;;) { }
}


void assert_failed(uint8_t* file, uint32_t line) {
	for(;;) {}
}
