#include <sys/time.h>

#include "stm32f10x_conf.h"
#include "config.h"

/**
 * @brief Init RTC clock module.
 * Init RTC with 32.768 KHz LSE oscilator.
 * @return nonzero if the clock configuration have been lost.
 **/
int rtc_init(void) 
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
	// RTC clock configuration: form SPL example
	if (BKP_ReadBackupRegister(RTC_BKP_REG) != 0xA5A5)
	{
		// RTC is not configured yet
		//PWR_BackupAccessCmd(ENABLE);
		PWR->CR |= PWR_CR_DBP;
		// Setup RTC source
		RCC_LSEConfig(RCC_LSE_ON);
		while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET) {}
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
		RCC_RTCCLKCmd(ENABLE);
		RTC_WaitForSynchro();
		RTC_WaitForLastTask();
		// Setup prescaler
		RTC_SetPrescaler(32767); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */
		RTC_WaitForLastTask();

		// configuration is finished
		BKP_WriteBackupRegister(RTC_BKP_REG, 0xA5A5);
		//PWR_BackupAccessCmd(DISABLE);
		PWR->CR &= ~PWR_CR_DBP;

		return -1;
	}
	return 0;
}

/**
 * @brief System call implementation for newlib.
 * @p tv_usec is always zero.
 **/
int _gettimeofday(struct timeval *tv, void *tz) 
{
	if (tv != NULL) {
		RTC_WaitForSynchro();
		tv->tv_sec = RTC_GetCounter();
		tv->tv_usec = 0;
	}
	return 0;
}

/**
 * @brief System call implementation.
 * TODO newlib support
 **/
int settimeofday(const struct timeval *tv, const struct timezone *tz) 
{
	if (tv != NULL) {
		//PWR_BackupAccessCmd(ENABLE);
		PWR->CR |= PWR_CR_DBP;
		RTC_WaitForLastTask();
		RTC_SetCounter(tv->tv_sec);
		RTC_WaitForLastTask();
		//PWR_BackupAccessCmd(DISABLE);
		PWR->CR &= ~PWR_CR_DBP;
	}
	return 0;
}

