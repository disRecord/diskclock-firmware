#ifndef  SEMAPHORE_H
#define  SEMAPHORE_H

#include "bool.h"

typedef enum {
	sem_unlocked = 0,
	sem_locked = 1,
} semaphore_t;

inline BOOL sem_try_lock(semaphore_t * sem) {
	do { 
		if (__LDREXB(sem) == sem_locked) {
			__CLREX();
			return FALSE;
		}
	} while (__STREXB(sem_locked, sem));
	return TRUE;
}

inline void sem_wait_lock(semaphore_t * sem) {
	semaphore_t old_value;
	do { 
		old_value =  __LDREXB(sem);
	} while (old_value == sem_locked && __STREXB(sem_locked, sem));
}

inline void sem_free(semaphore_t * sem) {
	*sem = sem_unlocked;
}

#endif  /*SEMAPHORE_H*/
