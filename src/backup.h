#ifndef  BACKUP_H
#define  BACKUP_H

#include "bool.h"
#include "config.h"

/*inline BOOL backup_is_consistent(void) {
	return BKP_ReadBackupRegister(BACKUP_CHECK_REG) == BACKUP_CHECK_VALUE;
}
*/

/**
 * @brief Check if backup storage is consistent.
 * @return TRUE if backup storage is consistent, FALSE otherwise.
 **/
#define backup_is_consistent() (BKP_ReadBackupRegister(BACKUP_CHECK_REG) == BACKUP_CHECK_VALUE)

void backup_init(void);

void backup_read(void * dst, uint16_t base_reg, int size);

void backup_write(uint16_t base_reg, void * src, int size);

void backup_set_consistent(void);


#endif  /*BACKUP_H*/
