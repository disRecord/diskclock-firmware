#include "video.h"


static const video_t * video;
static int current_frame_shift;

void video_nextframe() 
{
	diskd_frame_geometry_t geom = diskd_get_frame_geometry(0); 
	int frame_pixel_index = current_frame_shift;
	int row, col;

	// acquare frame buffer
	diskd_pixel_color_t * buffer = diskd_get_framebuffer();
	if (!buffer) return; // TODO waiting for buffer

	//  copy next frame to frame buffer
	switch (video->store_mode) {
		case COLS:
			// Not implemented yet.
			// TODO Implement COLS video store mode.
			break;

		case ROWS:
			for(row = 0; row < geom.rows; row++) {
				for(col = 0; col < geom.cols; col++) {
					buffer[row + col*geom.rows].raw = *((uint8_t*)video->data + frame_pixel_index++);
					if (frame_pixel_index == video->data_size) {
						// end of video, cycle
						frame_pixel_index = 0;
					}
				}
			}
			break;
	}
	diskd_release_framebuffer(FALSE);

	//shift to next frame and cycle
	current_frame_shift += video->frame_shift;
	if (current_frame_shift >= video->data_size) current_frame_shift = 0;
}

void video_init(const video_t * _video) 
{
	diskd_framebuffer_callback(0);  
	video = _video;
	current_frame_shift = 0;
	diskd_framebuffer_callback(video_nextframe);
}

void video_deinit(void) 
{
	diskd_framebuffer_callback(0);
}

