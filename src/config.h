#ifndef  CONFIG_H
#define  CONFIG_H

// IRQ priorities
#define DISKD_IRQ_PRIORITY 0
#define CLOCK_IRQ_PRIORITY 3

// backup registers
#define RTC_BKP_REG			BKP_DR1
#define COLOR_BKP_REG		BKP_DR2

#define BACKUP_CHECK_REG		BKP_DR3
#define BACKUP_CHECK_VALUE		0xA1A1
#define BACKUP_DISKD_BASE_REG	BKP_DR11

#endif  /*CONFIG_H*/
