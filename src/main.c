#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#include "stm32f10x_conf.h"

#include "led.h"
#include "uart1.h"
#include "rtc.h"
#include "audio.h"

#include "disk_display.h"
#include "clock.h"
#include "backup.h"

#include "video.h"
#include "video_clips.h"

void menu_failure(void) {
	puts("Operation failed.\n");
}

void menu_ok(void) {
	puts("OK.\n");
}


void menu_set_time()
{
	struct timeval tv;
	struct tm ts;

	fputs("Enter date (DD MM YYYY): ", stdout);
	if (scanf("%d %d %d", &ts.tm_mday, &ts.tm_mon, &ts.tm_year) != 3) {
		menu_failure(); return;
	}
	// fix bug 2000
	ts.tm_year -= 1900;
	fputs("Enter time (HH MM SS): ", stdout);
	if (scanf("%d %d %d", &ts.tm_hour, &ts.tm_min, &ts.tm_sec) != 3) {
		menu_failure(); return;
	}
	ts.tm_isdst = 0;

	tv.tv_usec = 0;
	tv.tv_sec = mktime(&ts);
	if (tv.tv_sec == (time_t) -1) {
		menu_failure(); return;
	}

	settimeofday(&tv, 0);

	menu_ok();
}

void menu_set_color(void)
{
	uint16_t R, G, B;
	diskd_pixel_color_t fg, bg;

	fputs("Enter foreground color (R G B, 0 <= R,G,B <= 3): ", stdout);
	if (scanf("%hu %hu %hu", &R, &G, &B) != 3) {
		menu_failure(); return;
	}
	fg.R = R; fg.G = G; fg.B = B;

	fputs("Enter background color (R G B, 0 <= R,G,B <= 3): ", stdout);
	if (scanf("%hu %hu %hu", &R, &G, &B) != 3) {
		menu_failure(); return;
	}
	bg.R = R; bg.G = G; bg.B = B;

	clock_set_color( fg, bg );

	menu_ok();
}

void menu_config_diskd(void)
{
	int16_t c;
	puts("Display parameters will be reconfigured.");

	fputs("Are you undestand what are you doing? (1/0)?: ", stdout);
	scanf("%hd", &c);
	if (c != 1) {
		menu_failure(); return;
	}

	diskd_parameters_t params;
	float tmp;
	uint16_t ui_tmp;

	diskd_parameters_init(&params);

	printf("Enter mot_freq (%f): ", (float) params.mot_freq);
	scanf("%f", &tmp);
	params.mot_freq = tmp;

	printf("Enter mot_max_accel (%f): ", (float) params.mot_max_accel );
	scanf("%f", &tmp);
	params.mot_max_accel = tmp;

	printf("Enter mot_max_accel_rate (%f): ", (float) params.mot_max_accel_rate);
	scanf("%f", &tmp);
	params.mot_max_accel_rate = tmp;

	printf("Enter base amplitude (%f): ", (float) params.mot_base_amp);
	scanf("%f", &tmp);
	params.mot_base_amp = tmp;

	printf("Enter vel amplitude (%f): ", (float) params.mot_vel_amp);
	scanf("%f", &tmp);
	params.mot_vel_amp = tmp;

	printf("Enter slide delta (%f): ", (float) params.mot_slide_delta);
	scanf("%f", &tmp);
	params.mot_slide_delta = tmp;

	printf("Enter disp_delta (%f): ", (float) params.disp_delta);
	scanf("%f", &tmp);
	params.disp_delta = tmp;

	printf("Set display force color in hex format (0): ");
	scanf("%hx", &ui_tmp);
	params.disp_forced_bright_color.raw = ui_tmp;

	puts("Changes will be applyed after reboot.");

	backup_write(BACKUP_DISKD_BASE_REG, &params, sizeof(params));
	
	menu_ok();
}


void menu(void) 
{
	int16_t selection;
	time_t tval;
	char * str;
	int i;

	tval  = time(NULL);
	str = ctime(&tval);

	puts(	"\nDisk Clock configuration menu");
	printf( "Current time: %s\n", str);
	fputs(	"\n"
			" 1 - set time and date,\n"
			" 2 - set display color,\n"
			" 3 - configure display parameters,\n"
			" 4 - enable sound, \n"
			" 5 - show picture, \n"
			" 6 - show time, \n"
			"\n" 
			"Enter your choice: ", stdout);

	if (scanf("%hd", &selection) != 1) {
		selection = -1;
	}

	switch(selection) {
		case 1:
			menu_set_time();
			break;
		case 2: 
			menu_set_color();
			break;
		case 3: 
			menu_config_diskd();
			break;
		case 4: 
			audio_set_noise(TRUE);
			menu_ok();
			break;
		case 5: 
			clock_deinit();	
			video_init(&clips_cutie_marks);
			menu_ok();
			break;
		case 6: 
			video_deinit();
			clock_init();	
			menu_ok();
			break;
		case 7: 
			printf("video_data_size: %d\n", clips_cutie_marks.data_size);
			printf("\nrow 1 [i]\n");
			for(i = 0; i < 40; i++) printf("%x ", (uint16_t) clips_cutie_marks.data[i].raw);
			printf("\nrow 2 [i]\n");
			for(i = 40; i < 80; i++) printf("%x ",(uint16_t) clips_cutie_marks.data[i].raw);
			printf("\nrow 1 *\n");
			for(i = 0; i < 40; i++) printf("%x ", (uint16_t) *((uint8_t*)clips_cutie_marks.data + i));
			printf("\nrow 2 *\n");
			for(i = 40; i < 80; i++) printf("%x ",(uint16_t) *((uint8_t*)clips_cutie_marks.data + i));
			printf("\nrow 3 *\n");
			for(i = 80; i < 120; i++) printf("%x ",(uint16_t) *((uint8_t*)clips_cutie_marks.data + i));
			menu_ok();
			break;
		case -1:
		default:
			menu_failure();
			break;
	}
};


int main()
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	led_init();
	uart1_init();	
	backup_init();
	audio_init();

	{
		diskd_parameters_t params;
		diskd_parameters_init(&params);

		if (backup_is_consistent()) {
			backup_read(&params, BACKUP_DISKD_BASE_REG, sizeof(params));
		}
		else {
			backup_write(BACKUP_DISKD_BASE_REG, &params, sizeof(params));
			backup_set_consistent();
		}

		diskd_init(&params);
		clock_init();
		//video_init(&clips_cutie_marks);
	}

	for(;;) {
		menu();
	}

	return 0;
}

void assert_failed(uint8_t* file, uint32_t line) {
	dprintf(fileno(stderr), "Assertion failed in file \"%s\", line %lu.\n", file, line);
	fflush(stderr);
}
