#include <time.h>

#include "stm32f10x_conf.h"

#include "config.h"
#include "font.h"
#include "rtc.h"
#include "disk_display.h"

static diskd_pixel_color_t clock_color;
static diskd_pixel_color_t clock_bg_color;

void clock_init(void) 
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

	// init RTC clock if necessary
	rtc_init();

	// Load color from BKP register
	uint16_t tmp;
	tmp = BKP_ReadBackupRegister(COLOR_BKP_REG);
	clock_color.raw = tmp;
	clock_bg_color.raw = tmp >> 8;
	if (clock_color.raw == clock_bg_color.raw) { 
		clock_color.raw = DISKD_COLOR_GREEN;
		clock_bg_color.raw = DISKD_COLOR_BLACK;
	}
	
	// Configure RTC_SEC interrupt
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = CLOCK_IRQ_PRIORITY;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	// Enable secoond counter interrupts
	RTC_WaitForLastTask();
	RTC_ITConfig(RTC_IT_SEC, ENABLE);
	RTC_WaitForLastTask();
}

void clock_deinit(void) 
{
	RTC_WaitForLastTask();
	RTC_ITConfig(RTC_IT_SEC, DISABLE);
}

void clock_set_color(diskd_pixel_color_t color, diskd_pixel_color_t bg_color) 
{
	uint16_t tmp;

	clock_color = color;
	clock_bg_color = bg_color;

	tmp = color.raw; tmp |= bg_color.raw << 8;
	//PWR_BackupAccessCmd(ENABLE);
	PWR->CR |= PWR_CR_DBP;
	BKP_WriteBackupRegister(COLOR_BKP_REG, tmp);
	//PWR_BackupAccessCmd(DISABLE);
	PWR->CR &= ~PWR_CR_DBP;
}

void clock_set_time(int h, int m, int s) {
	struct timeval tv;
	struct tm ts;

	ts.tm_sec = s;
	ts.tm_min = m;
	ts.tm_hour = h;
	ts.tm_mday = 0;
	ts.tm_mon = 0;
	ts.tm_year = 2000;
	ts.tm_isdst = 0;

	tv.tv_sec = mktime(&ts);
	tv.tv_usec = 0;

	settimeofday(&tv, 0);
}

void clock_display(void) 
{
	time_t tval;
	char * str;
	diskd_pixel_color_t * fb_ptr;
	diskd_frame_geometry_t fg;
	int row, k, symbol_col;

	font_symbol_t symbol;

	tval  = time(NULL);
	str = ctime(&tval);

	diskd_get_frame_geometry(&fg);
	fb_ptr = diskd_get_framebuffer();

	for(k = 0; k < fg.cols/FONT_WIDTH; k++) { // iterate over symbols
		symbol = font_get_symbol(str[11 + k]);

		for(symbol_col = 0; symbol_col < FONT_WIDTH; symbol_col++) { //iterate over symbol columns
			for(row = 0; row < fg.rows; row++) {
				if ( symbol[symbol_col] & ((uint16_t) 1 << row) ) {
					*fb_ptr = clock_color;
				}
				else {
					*fb_ptr = clock_bg_color;
				}
				fb_ptr++;
			}
		}
	}

	diskd_release_framebuffer(TRUE);
}

void RTC_IRQHandler(void) 
{
	if (RTC_GetITStatus(RTC_IT_SEC)) {

		clock_display();

		RTC_ClearITPendingBit(RTC_IT_SEC);
	}
}
