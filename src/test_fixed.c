#include <stdio.h>

#include "stm32f10x_conf.h"

#include "led.h"
#include "uart1.h"

int main() 
{
	float a, b, c;
	_Accum af, bf, cf;
	_Fract f1, f2;

	uart1_init();

	puts("\nEnter two numbers:");
	scanf("%f %f", &a, &b);

	af = a; bf = b;

	c = a+b;
	cf = af+bf;
	printf("a + b: %f, %f\n", c, (float) cf);

	c = a * b;
	cf = af * bf;
	printf("a * b: %f, %f\n", c, (float) cf);

	c = a / b;
	cf = af / bf;
	printf("a / b: %f, %f\n", c, (float) cf);

	f1 = 0.5r;
	f2 = 1.0r;
	printf("1.0 = %f, 0.5+0.5 = %f\n", (float) f2, (float) (f1 + f1));

	fflush(stdout);

	return 0;
}

void assert_failed(uint8_t* file, uint32_t line) {
	for(;;) {}
}
