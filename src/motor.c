#include "stm32f10x_conf.h"

#include "disk_display_config.h"
#include "motor.h"


/**
   @brief Initialize motor control.
*/
void motor_init(void) 
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_BaseInit;
	TIM_OCInitTypeDef TIM_OCInit;

	RCC_APB2PeriphClockCmd(MOT_APB2Periph, ENABLE);
	RCC_APB1PeriphClockCmd(MOT_APB1Periph, ENABLE);

	// MOTOR CONTROL SETUP
	//
	// Configure A, B, C output pins
	GPIO_InitStructure.GPIO_Pin = MOT_A | MOT_B | MOT_C;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_ResetBits( MOT_PORT, MOT_A | MOT_B | MOT_C );	// Pull all phases to zero
	GPIO_Init( MOT_PORT , &GPIO_InitStructure);

	// protect from button press (pull up resistor is present)
	GPIO_InitStructure.GPIO_Pin = MOT_C;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_Init( MOT_PORT , &GPIO_InitStructure);
	
	// Configure EN signal pin. 
	GPIO_InitStructure.GPIO_Pin = MOT_EN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	// Disable motor driver.
	GPIO_ResetBits( MOT_EN_PORT, MOT_EN );
	GPIO_Init( MOT_EN_PORT , &GPIO_InitStructure);

	// Init PWM timer 
	TIM_BaseInit.TIM_CounterMode = TIM_CounterMode_CenterAligned3; // Events are generated when counter is counting up and down.
	TIM_BaseInit.TIM_Period = MOT_TIMER_PERIOD-1; 
	TIM_BaseInit.TIM_Prescaler = 0;
	TIM_BaseInit.TIM_ClockDivision = 0;
	TIM_BaseInit.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit( MOT_TIMER, &TIM_BaseInit );
	// Allow update buffered register content
	TIM_UpdateDisableConfig( MOT_TIMER, DISABLE );
	// Setup Output Compare mode: PWM2 --- center aligned: 
	// output is high if CCR < CNT when timer is counting up.
	// output is low if CCR > CNT when timer is counting down.
	// Common configuration:
	TIM_OCStructInit(&TIM_OCInit);
	TIM_OCInit.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInit.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInit.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInit.TIM_Pulse = 0;
	
	// MOT_A --- CH4
	TIM_OC4Init( MOT_TIMER, &TIM_OCInit);
	TIM_OC4PreloadConfig( MOT_TIMER, TIM_OCPreload_Enable);
	TIM_CCxCmd( MOT_TIMER, TIM_Channel_4, TIM_CCx_Enable );
	// MOT_B --- CH3
	TIM_OC3Init( MOT_TIMER, &TIM_OCInit);
	TIM_OC3PreloadConfig( MOT_TIMER, TIM_OCPreload_Enable);
	TIM_CCxCmd( MOT_TIMER, TIM_Channel_3, TIM_CCx_Enable );
	// MOT_C --- CH2
	TIM_OC2Init( MOT_TIMER, &TIM_OCInit);
	TIM_OC2PreloadConfig( MOT_TIMER, TIM_OCPreload_Enable);
	TIM_CCxCmd( MOT_TIMER, TIM_Channel_2, TIM_CCx_Enable );

	TIM_Cmd( MOT_TIMER, ENABLE);
}

/**
 * @brief Switch motor control signal phase and magnitude.
 * Set space vector modulation PWM value according to the DIH method.
 * @param phase is angle of SVM can take values from 0 to @p MOT_N_SVM_PHASES.
 * @param magintude can take values from 0 to 1.0.
 **/
void motor_set_svm(_Accum amplitude, uint16_t phase) 
{
	const _Fract ref_func[MOT_N_SVM_PHASES] =  { 0.43301r, 0.48296r, 0.50000r, 0.48296r, 0.43301r, 0.22414r, 0.00000r, -0.22414r, -0.43301r, -0.48296r, -0.50000r, -0.48296r, -0.43301r, -0.48296r, -0.50000r, -0.48296r, -0.43301r, -0.22414r, 0.00000r, 0.22414r, 0.43301r, 0.48296r, 0.50000r, 0.48296r,  };
	_Accum A, B, C;
	
	if (phase < MOT_N_SVM_PHASES) {
		A = 0.5k + amplitude*ref_func[phase];
		phase += MOT_N_SVM_PHASES/3; if (phase >= MOT_N_SVM_PHASES) phase -= MOT_N_SVM_PHASES;
		C = 0.5k +  amplitude*ref_func[phase];
		phase += MOT_N_SVM_PHASES/3; if (phase >= MOT_N_SVM_PHASES) phase -= MOT_N_SVM_PHASES;
		B = 0.5k +  amplitude*ref_func[phase];

		MOT_A_TIMER_OCR = A << MOT_TIMER_PERIOD_2POWER;
		MOT_B_TIMER_OCR = B << MOT_TIMER_PERIOD_2POWER;
		MOT_C_TIMER_OCR = C << MOT_TIMER_PERIOD_2POWER;
	}
}

/**
 * @brief Enable motor.
 * Switch motor driver on.
 **/
void motor_enable(void) 
{
	GPIO_SetBits( MOT_EN_PORT, MOT_EN );
}

/**
 * @brief Disable motor.
 * Switch motor driver off.
 **/
void motor_disable(void) 
{
	GPIO_ResetBits( MOT_EN_PORT, MOT_EN );
}

/**
 * @brief Brake motor.
 * Short circuit the motor windings.
 **/
void motor_brake(void) 
{
	MOT_A_TIMER_OCR = 0;
	MOT_B_TIMER_OCR = 0;
	MOT_C_TIMER_OCR = 0;
}

/**
   @brief Deconfigure motor.
*/
void motor_deinit(void) 
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_ResetBits( MOT_EN_PORT, MOT_EN );

	TIM_DeInit(MOT_TIMER);

	// Deconfigure A, B, C output pins
	GPIO_StructInit( &GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Pin = MOT_A | MOT_B | MOT_C;
	GPIO_Init( MOT_PORT, &GPIO_InitStructure);
	
}
