#include "disk_display_config.h"
#include "ekf.h"


/**
 * @brief Extended Kalman filter for motor velocity and acceleration estimation.
 * Modify previous acceleration and velocity estimation @p prev_estimate with new measure @p T.
 * Velocity is measured in revolutions per cycle timer period.
 * @param prev_estimate Pointer to sructure @a ekf_state_t, which contains previous estimation. Previous estimation is replaced by new.
 * @param T_m Measured period length, takes values up to @a TIMER_TOP.
 * @return New estimation.
 **/
ekf_state_t ekf_filter(ekf_state_t * prev_estimate, _Accum T_m)
{
	_Accum vel_m, nu;
	uint16_t vel_index;

	// precalculated alpha-beta Kalman filter coefficient for different measurement frequencies: 1 Hz, 2Hz, ... 60 Hz
	// Noise mean errors: 
	// measurement: 0.005 (fraction of timer period)
	// object: 10 (Hz^3, acceleration noise) 
	const _Accum alpha[] =   { 0.999630k, 0.958719k, 0.784222k, 0.583906k, 0.430967k, 0.324437k, 0.250512k, 0.198170k, 0.160156k, 0.131851k, 0.110291k, 0.093532k, 0.080270k, 0.069609k, 0.060917k, 0.053743k, 0.047756k, 0.042709k, 0.038417k, 0.034737k, 0.031559k, 0.028796k, 0.026379k, 0.024253k, 0.022373k, 0.020703k, 0.019212k, 0.017877k, 0.016675k, 0.015591k, 0.014608k, 0.013716k, 0.012902k, 0.012159k, 0.011478k, 0.010853k, 0.010277k, 0.009746k, 0.009255k, 0.008800k, 0.008378k, 0.007985k, 0.007619k, 0.007278k, 0.006959k, 0.006661k, 0.006382k, 0.006119k, 0.005873k, 0.005641k, 0.005422k, 0.005216k, 0.005022k, 0.004838k, 0.004664k, 0.004499k, 0.004343k, 0.004195k, 0.004054k, 0.003921k,  };
	const _Accum beta[] =  { 1.923789k, 2.539709k, 1.720440k, 1.007896k, 0.603474k, 0.380522k, 0.252399k, 0.174893k, 0.125711k, 0.093175k, 0.070867k, 0.055098k, 0.043652k, 0.035152k, 0.028713k, 0.023749k, 0.019862k, 0.016777k, 0.014297k, 0.012281k, 0.010626k, 0.009255k, 0.008110k, 0.007146k, 0.006328k, 0.005630k, 0.005031k, 0.004514k, 0.004066k, 0.003675k, 0.003332k, 0.003031k, 0.002765k, 0.002529k, 0.002319k, 0.002132k, 0.001964k, 0.001814k, 0.001678k, 0.001556k, 0.001445k, 0.001344k, 0.001253k, 0.001170k, 0.001094k, 0.001024k, 0.000960k, 0.000901k, 0.000847k, 0.000798k, 0.000752k, 0.000709k, 0.000670k, 0.000634k, 0.000600k, 0.000568k, 0.000539k, 0.000511k, 0.000486k, 0.000462k,  };

	//TODO: remove macro and made independent of DISKD module
	//measured velocity
	vel_m = (_Accum) DISKD_TIMER_TOP / T_m;
	// convert velocity to Hz and calculate kalman filter coeficient index
	vel_index = (int) (vel_m << DISKD_TIMER_FREQ_2POWER);
	if (vel_index > 59) vel_index = 59;
	else if (vel_index < 0) vel_index = 0;

	//predicted velocity
	prev_estimate->vel += (T_m >> DISKD_TIMER_TOP_2POWER) * prev_estimate->accel;
	//innovation 
	nu = vel_m - prev_estimate->vel;
	//aposteriory estimation
	prev_estimate->vel += alpha[vel_index] * nu;
	prev_estimate->accel += beta[vel_index] * nu;

	return *prev_estimate;
}


