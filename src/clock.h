#ifndef  CLOCK_H
#define  CLOCK_H

void clock_init(void);

void clock_deinit(void);

void clock_set_color(diskd_pixel_color_t color, diskd_pixel_color_t bg_color);

void clock_set_time(int h, int m, int s);

#endif  /*CLOCK_H*/
