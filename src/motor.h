#ifndef  MOTOR_H
#define  MOTOR_H

void motor_init(void);

void motor_enable(void);

void motor_disable(void);

void motor_brake(void);

void motor_set_svm(_Accum amplitude, uint16_t phase);

void motor_deinit(void);

#endif  /*MOTOR_H*/
