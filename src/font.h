#ifndef  FONT_H
#define  FONT_H

#include <stdint.h>

#define FONT_WIDTH 5

/**
 * @brief Font symbol representation.
 * TODO Fill description.
 */
typedef const uint16_t* font_symbol_t;

/**
 * @brief Get symbol.
 **/
font_symbol_t font_get_symbol(uint16_t c);

#endif  /*FONT_H*/


