nS = 5;
NpS = 6000;
N = nS*NpS;
t = linspace(0, nS, N)';

% parameters
sigma_w = 5;
sigma_v = 0.0005;

x0 = [0 0 1];

% create sytem model
A = [ 0 1 0; 0 0 1; 0 0 0];
B = [ 0; 0; 1];
C = eye([3 3]);
sys = ss(A, B, C);

w = sigma_w*randn([N 1]);

% modelling system
x = lsim(sys, w, t, x0);
phi = x(:, 1);

%imitate tachometer
rev = floor(phi);
srev = shift(rev, 1); srev(1) = floor(x0(1));
rev = rev - srev;
T = [0; find(rev)];
