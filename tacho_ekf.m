
% EKF for momentary angular speed estimation

nT = length(T);
omega = zeros(size(T)); theta = zeros(size(T)); nu = zeros(size(T)); p11 = zeros(size(T));
omega_s = zeros(size(T)); theta_s = zeros(size(T)); nu_s = zeros(size(T));
lambda = zeros(size(T)); a = zeros(size(T)); b = zeros(size(T));
% filter initial conditions
sigma_f_v = 0.010; % sensor noise: imitate 
sigma_f_w = 1;

omega(1) = 0.5;
theta(1) = 1;

for k = 1:nT-1
	D = 1/omega(k);
	%D = DT(k)/NpS;
	
	lambda(k+1) = sigma_f_w/sigma_f_v * D^4;
	a(k+1) = -1/8*(lambda(k+1)^2 + 8*lambda(k+1) - (lambda(k+1)+4)*sqrt(lambda(k+1)^2 + 8*lambda(k+1)));
	b(k+1) = 1/4*(lambda(k+1)^2 + 4*lambda(k+1) - lambda(k+1)*sqrt(lambda(k+1)^2 + 8*lambda(k+1)));
	% prediction phase
	omega(k+1) = omega(k) + D*theta(k);
	theta(k+1) = theta(k);
	% correction phase
	nu(k+1) = (NpS / DT(k)) - omega(k+1);
	p11(k+1) = a(k+1)*sigma_f_v^2/D^4;
	theta(k+1) = theta(k+1) + b(k)/D*nu(k+1);
	omega(k+1) = omega(k+1) + a(k)*nu(k+1);

	% simple estimation scheme
	if (k > 1)
		theta_s(k+1) = (NpS/DT(k) - NpS/DT(k-1))/(DT(k)/NpS);
	else
		theta_s(k+1) = 0;
	end
	omega_s(k+1) = NpS/DT(k) + theta_s(k+1)*(DT(k)/NpS);

	nu_s(k+1) = (NpS / DT(k)) - omega_s(k);
end
