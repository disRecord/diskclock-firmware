" определяем путь к папке .vimprj
let s:sPath = expand('<sfile>:p:h')

" указываем плагину indexer файл проекта, который лежит в папке .vimprj
let g:indexer_projectsSettingsFilename = s:sPath.'/.vimprojects'

" указываем плагину project файл проекта, который лежит в папке .vimprj
" ВНИМАНИЕ: этой переменной нет в стандартном project.
" Вам нужно немного изменить файл project.vim, чтобы это сработало
" об этом чуть ниже
let g:proj_project_filename=s:sPath.'/.vimprojects'

" можно еще, например, указать специфический для этого проекта компилятор
"let &makeprg = 'pic30-gcc -mcpu=24HJ128GP504 -x c -c "%:p" -o"%:t:r.o" -I"'.$INDEXER_PROJECT_ROOT.'\src\utils" -I"'.$INDEXER_PROJECT_ROOT.'\src\app" -g -Wall -mlarge-code -mlarge-data -O1'

"Изложенное точно верно для версии project 1.4.1 (последняя версия на момент написания данной статьи).
"Перейдите на строку 1272. В ней должна быть только одна команда:
"
"Project
"
"Необходимо заменить эту строку на следующий блок кода:
"
"if !exists("g:proj_running") && exists('g:proj_project_filename')
"   exec('Project '.g:proj_project_filename)
"else
"   Project
"endif
