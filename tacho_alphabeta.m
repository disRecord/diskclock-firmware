
a = []; b = [];
for w = 1:60
	lambda = sigma_f_w / sigma_f_v / w^4
	a(w) = -1/8*(lambda^2 + 8*lambda - (lambda+4)*sqrt(lambda^2 + 8*lambda));
	b(w) = 1/4*(lambda^2 + 4*lambda - lambda*sqrt(lambda^2 + 8*lambda));
	b(w) *= w;
end

alpha_str = cstrcat(' { ', sprintf('%0.6fk, ', a), ' };');
beta_str = cstrcat(' { ', sprintf('%0.6fk, ', b), ' };');

alpha_str
beta_str


