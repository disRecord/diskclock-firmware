nS = 15; % experiment duraton
NpS = 6000; % tachometer timer resolution
N = nS*NpS;
t = linspace(0, nS, N)';

% noise parameters
sigma_w = 0; % model noise (1/s^3)? acceleration variation
sigma_v = 0.0005; % sensor noise: 

x0 = [0 1.5 1];

% create sytem model
A = [ 0 1 0; 0 0 1; 0 0 0];
B = [ 0; 0; 1];
C = eye([3 3]);
sys = ss(A, B, C);

w = sigma_w*randn([N 1]);

% modelling system
x = lsim(sys, w, t, x0);
phi = x(:, 1);

%imitate tachometer
rev = floor(phi);
srev = shift(rev, 1); srev(1) = floor(x0(1));
rev = rev - srev;
% T contains times of tacho pulses
T = [0; find(rev)];
% measured intervals beetween tacho pulses
DT = diff(T) + round(randn([rows(T)-1 1])*(sigma_v*NpS));
